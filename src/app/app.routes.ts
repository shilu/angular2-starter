import { Routes } from '@angular/router';
import { DefaultMainComponent } from './common/default-main';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { AuthGuard }  from './login/auth-guard.service';

export const ROUTES: Routes = [
  { path: 'home',  component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent },
  { path: 'wms', loadChildren: './wms#WmsModule',  canActivate: [AuthGuard]},
  { path: '',   redirectTo: 'login', pathMatch: 'full' },
  { path: '**',    redirectTo: 'login'}
];
