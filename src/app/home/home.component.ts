import {Component, OnInit} from '@angular/core';
// import { AppState } from '../app.service';
// import { Title } from './title';
// import { XLargeDirective } from './x-large';
import { SessionService } from 'app/common/service/session.service';
import { UserService } from 'app/common/service/user.service';
import { MODULE_CONFIG } from 'app/common/data/module_config'
import * as _ from "lodash";
@Component({
  selector: 'home', 
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  user:object;
  modules: object[];
  constructor(private sessionService: SessionService, private userService: UserService) {}
  public ngOnInit() {
    this.modules = MODULE_CONFIG;
    console.log(MODULE_CONFIG);
    // this.sessionService.getUserInfo().then(function (userInfo) {
    //   this.user = userInfo;
    //   var userRoles = _.map(userInfo.roles, 'name');
    //   $scope.modules = _.filter(moduleConfig, function (module) {
    //       return module.label;
    //   });
    // }); 
  }
    // console.log('hello `Home` component');
    // /**
    //  * this.title.getData().subscribe(data => this.data = data);
    //  */
  

}
