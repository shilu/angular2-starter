import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
// import { Observable } from 'rxjs';
// import 'rxjs';
import * as _ from "lodash";
// import { Observable } from '@reactivex/rxjs';
import { SessionService } from 'app/common/service/session.service';
import { DialogService } from 'app/common/service/dialog.service';
import { UserService } from 'app/common/service/user.service';
import { CompanyAndFacilityDisplayService } from 'app/common/service/companyAndFacilityDisplay.service';
@Injectable()
export class AuthService {
    // redirectUrl:string;
    constructor(
      private http:HttpClient, 
      private sessionService:SessionService, 
      private dialogService:DialogService,
      private userService:UserService,
      private companyAndFacilityDisplayService:CompanyAndFacilityDisplayService) {};
    signIn(data, fn) {
      this.http.post('/shared/idm-app/user/login',  data).subscribe(
        res => {
          if(res["success"]){
            // $http.defaults.headers.common.user = userName;
            this.sessionService.setUserToken(res["oAuthToken"]);
            this.sessionService.setUserId(res["idmUserId"]);
            this.sessionService.setUserPermission(_.map(res["userPermissions"], 'name'));
            this.setUserAndCompanyFacilityToSession(res["idmUserId"], function () {
              if(fn) fn();
            });
          }else {
            var errMessage = res["errorMessage"] ? res["errorMessage"]: "Login Failed!";
            this.dialogService.errorDialog(errMessage, null);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            this.dialogService.errorDialog("Username or password incorrect.", null);
            // lincUtil.errorPopup();
            console.log("Client-side error occured.");
          } else {
            this.dialogService.errorDialog("Internal Server Error, please contact IT..", null);
            // lincUtil.errorPopup("Internal Server Error, please contact IT.");
            console.log("Server-side error occured.");
          }
        }
      );
    }

    private setUserAndCompanyFacilityToSession(userId, cbFun) {
      console.log(userId);
      this.userService.getUserDetailById(userId).subscribe(
        res => {
          this.sessionService.setUserInfo(res);
          this.sessionService.setAssignedCompanyFacilities(res["assignedCompanyFacilities"]);
          var defaultCf = this.companyAndFacilityDisplayService.getDefaultCompanyFacility(res["defaultCompanyFacility"], res["assignedCompanyFacilities"]);
          this.sessionService.setCompanyFacility(defaultCf);
          cbFun();
        },
        (err: HttpErrorResponse) => {
          this.dialogService.errorDialog("Internal Server Error, please contact IT.", null);
        }
      );
    }
    
    signOut() {
      this.http.post('/shared/idm-app/user/logout', {oauthToken: this.sessionService.getUserToken()}).subscribe(
        res => {
          this.sessionService.clean();
        },
        (err: HttpErrorResponse) => {
          this.sessionService.clean();
        }
      )
    }

    isSignIn = function() {
			return this.sessionService.getUserId();
		};
  }
