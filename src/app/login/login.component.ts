import { Component, Input, OnInit } from '@angular/core';
// import { ActivatedRoute, ParamMap} from '@angular/router';
// import { Location } from '@angular/common';
// import { UserService } from './hero.service';
import { AuthService } from './auth.service';
// import { AuthGuard } from './auth-guard.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router} from '@angular/router';

// import 'rxjs/add/operator/switchMap';
// import { Hero } from './hero';
@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.css' ]
})

export class LoginComponent{
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder, 
    private authService: AuthService,
    private router: Router) {
      this.authService.signOut();
      this.createForm();
  };

  createForm() {
    this.loginForm = this.fb.group({
       username: ['', Validators.required ],
       password: ['', Validators.required ],
    });
  }

  signIn() {
    const loginFormModel = this.loginForm.value;
    loginFormModel.returnUserPermissions = ["WEB"];
    this.authService.signIn(loginFormModel, ()=>{
      // let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/home';
      this.router.navigate(['/home']);
    });
  };
} 