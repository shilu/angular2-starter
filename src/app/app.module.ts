import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
// import { HttpClientModule} from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatDialogModule, MatButtonModule, MatIconModule } from '@angular/material';

import { ROUTES } from './app.routes';


// App is our top level component
import { AppComponent } from './app.component';
import { LoginRoutingModule, LoginComponent } from './login';
import { HomeComponent } from './home';
import { DefaultMainComponent } from './common/component/default-main';
import { AlertDialogComponent } from './common/component/alert-dialog'

import { SessionService } from "./common/service/session.service";
import { DialogService } from "./common/service/dialog.service";
import { UserService } from 'app/common/service/user.service';
// import { AuthService } from 'app/common/service/auth.service';
// import { AuthGuard } from './login/auth-guard.service'; 
import { CompanyService } from 'app/common/service/company.service';
import { FacilityService } from 'app/common/service/facility.service';
import { UtilService } from 'app/common/service/util.service';
import { CompanyAndFacilityDisplayService } from 'app/common/service/companyAndFacilityDisplay.service';

import { TokenInterceptor } from 'app/common/service/tokenInterceptor';




//import module
// import { WmsModule } from './wms';

// import { NoC/ontentComponent } from './no-content';
// import { XLargeDirective } from './home/x-large';
// import { DevModuleModule } from './+dev-module';

// import '../styles/styles.scss';
// import '../styles/headings.css';

// Application wide providers
// const APP_PROVIDERS = [
//   ...APP_RESOLVER_PROVIDERS,
//   AppState
// ];

// type StoreType = {
//   state: InternalStateType,
//   restoreInputValues: () => void,
//   disposeOldHosts: () => void
// };

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent],
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DefaultMainComponent,
    AlertDialogComponent
  ],
  entryComponents: [
    AlertDialogComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    // BrowserAnimationsModule,
    // FormsModule,
    HttpClientModule,
    // WebStorageModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    LoginRoutingModule,
    RouterModule.forRoot(ROUTES, {
      // useHash: Boolean(history.pushState) === false,
       enableTracing: true
      // useHash: true,
      // preloadingStrategy: PreloadAllModules
    })
    // WmsModule
    /**
     * This section will import the `DevModuleModule` only in certain build types.
     * When the module is not imported it will get tree shaked.
     * This is a simple example, a big app should probably implement some logic
     */
    // ...environment.showDevModule ? [ DevModuleModule ] : [],
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    SessionService,
    DialogService,
    CompanyService,
    FacilityService,
    UtilService,
    CompanyAndFacilityDisplayService,
    // AuthGuard,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
    // environment.ENV_PROVIDERS,
    // APP_PROVIDERS
  ]
})
export class AppModule {}
