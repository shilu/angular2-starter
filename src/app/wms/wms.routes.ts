import { WmsComponent } from './wms.component';
import { ReceiptListComponent} from './inbound/receipt'

export var wmsRoutes = [
  {
    path: '',
    component: WmsComponent,
    children: [
      {
        path: 'inbound',
        children: [
          {
            path: 'receipt',
            children: [
              {
                path: 'list',
                component: ReceiptListComponent
              }
            ]
          }
        ]
      }
    ]
  }
];