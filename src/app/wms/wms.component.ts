import {
  Component,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'wms',
  template: 'Welcome to WMS<router-outlet></router-outlet>'
})
export class WmsComponent implements OnInit {

  public ngOnInit() {
    console.log('hello `wms` component');
  }

}
