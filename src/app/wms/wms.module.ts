import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { wmsRoutes } from './wms.routes';
import { WmsComponent } from './wms.component';
import { ReceiptListComponent} from './inbound/receipt'

@NgModule({
  declarations: [
    WmsComponent,
    ReceiptListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(wmsRoutes),
  ],
  exports: [
    RouterModule
  ]
})
export class WmsModule {
 
}
