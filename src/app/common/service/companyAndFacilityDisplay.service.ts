import { Injectable } from '@angular/core';
import { DISPLAY_CONFIG } from 'app/common/data/company_and_facility_header_display';
import * as _ from "lodash";

@Injectable()
export class CompanyAndFacilityDisplayService {
    getDisplayByStateName(stateName) {
        let displayConfigs = DISPLAY_CONFIG;
        var displayConfig = _.findLast(displayConfigs, function (config) {
            return stateName.indexOf(config["state"]) > -1;
        });
        if(displayConfig) {
            return displayConfig["display"];
        } else {
            return ["facility", "company"];
        }
    };

    getDefaultCompanyFacility(cf, assignedCfs) {
        let defaultCf = cf;
        if(assignedCfs && assignedCfs.length > 0) {
            if (!defaultCf) {
                defaultCf = assignedCfs[0];
            }
        }
        return defaultCf;
    };
}