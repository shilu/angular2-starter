import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AlertDialogComponent } from 'app/common/component/alert-dialog'
@Injectable()
export class DialogService {
    constructor(private dialog: MatDialog) {}

    errorDialog(message, cbFun = null) {
      this.messageDialog("Error", message, false, cbFun);
    }

    saveSuccessfulDialog(cbFun = null) {
      this.messageDialog("Message", "Save Successful.", false, cbFun);
    }

    updateSuccessfulDialog(cbFun = null) {
      this.messageDialog("Message", "Update Successful.", false, cbFun);
    }
  
    messageDialog(title, message, isConfirm:boolean = false, cbFun = null) {
        let dialogRef: MatDialogRef<AlertDialogComponent> = this.dialog.open(AlertDialogComponent, {
          width: '350px',
          // position: {right:"20%"},
          // disableClose: false,
          // role: "dialog"
        });
        dialogRef.componentInstance.dialogMessage = message;
        dialogRef.componentInstance.dialogTitle = title;
        dialogRef.componentInstance.isConfirm = false;
        if(cbFun) {
          dialogRef.afterClosed().subscribe(result => {
            console.log("fdsfsfds");
            dialogRef = null;
            cbFun();
          });
        }else {
          dialogRef = null;
        }
    }


    // processErrorDialog(response) {
    //   if (response.status && response.status === 400) {
    //       if (response.error) {
    //           this.errorDialog(response.error);
    //           return;
    //       }
    //       var err = response.data.data || response.data;
    //       if (err) {
    //           this.errorDialog(angular.fromJson(err).error);
    //           return;
    //       }
    //       this.errorDialog("Bad Request, please adjust your request and try it again.");
    //   }
    //   if (response.status && response.status === 500) {
    //       if (response.error) {
    //           this.errorDialog(response.error);
    //           return;
    //       }
    //       var erData = response.data.data || response.data;
    //       if (erData) {
    //           this.errorDialog(angular.fromJson(erData).error);
    //           return;
    //       }
    //       this.errorDialog("Server Internal Error, Please call the admin to fix it.");
    //   }
    //   if (response.status && response.status === 404) {
    //       this.errorDialog("Service you called is not found.");
    //   }
    //   if (response.status && response.status === 503) {
    //       this.errorDialog("Service is under system maintenance, please come back later.");
    //   }
    //   if (response.status && response.status === -1) {
    //       this.errorDialog("System connection time out, please try again later.");
    //   }
    // }

}
