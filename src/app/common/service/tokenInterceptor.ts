import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import { SessionService } from 'app/common/service/session.service';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private sessionService:SessionService) {};
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let updateObj = {};
    if(req.url.indexOf("jira") < 0 && this.sessionService.getUserToken()) {
      updateObj["headers"] = req.headers.set('Authorization', this.sessionService.getUserToken())
    }
    var authReq = req.clone(updateObj);
    return next.handle(authReq);
  }
}