import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

    public setUserInfo = function(userInfo) {
        this.setItemToStorage("userInfo", userInfo);
    };

    public getUserInfo = function() {
        return this.getItemFromStorage("userInfo");
    };
    

    public setUserId = function(userId) {
        this.setItemToStorage("userId", userId);
    };

    public getUserId = function() {
        return this.getItemFromStorage("userId");
    };
    
    public getUserToken() {
        return this.getItemFromStorage("token");
    };

    public setUserToken(token) {
        this.setItemToStorage("token", token);
    };

    public getUserPermission() {
         return this.getItemFromStorage("userPermissions");
    };

    public setUserPermission(userPermissions) {
       this.setItemToStorage("userPermissions", userPermissions);
    };


    getCompanyFacility() {
        return this.getItemFromStorage("companyFacility");
    };

    setCompanyFacility(companyFacility) {
        this.setItemToStorage("companyFacility", companyFacility);
    };

    getAssignedCompanyFacilities() {
        return this.getItemFromStorage("assignedCompanyFacilities");
    };

    setAssignedCompanyFacilities(assignedCompanyFacilities) {
        this.setItemToStorage("assignedCompanyFacilities", assignedCompanyFacilities);
    };


    private setItemToStorage(key, value) {
        if(value) {
            sessionStorage.setItem(key, JSON.stringify(value));
        }
    }

    private getItemFromStorage(key) {
        var item = sessionStorage.getItem(key);
        if (item !== null && typeof item === 'string'){
            var itemValue = null;
            try {
                itemValue = JSON.parse(item);
            } catch (err) {
            }
            return itemValue;
        } else {
            return item;
        }
    }

    public clean() {
        sessionStorage.clear();
    }
    
  }
