import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';

@Injectable()
export class CompanyService {
    constructor(private http:HttpClient) {};

    searchCompany(params) {
        return this.http.post('/shared/fd-app/company/search',  params);
    }
}