import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';

@Injectable()
export class FacilityService {
    constructor(private http:HttpClient) {};

    searchFacility(params) {
        return this.http.post('/shared/fd-app/facility/search',  params);
    }
}
