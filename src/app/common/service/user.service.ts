import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import * as _ from "lodash";
import {  Observable } from 'rxjs';
import * as Rx from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CompanyService } from 'app/common/service/company.service';
import { FacilityService } from 'app/common/service/facility.service';
import { UtilService } from 'app/common/service/util.service';

@Injectable()
export class UserService {
    constructor(
        private http: HttpClient, 
        private companyService: CompanyService,
        private facilityService: FacilityService,
        private utilService: UtilService
    ) {

    };

    getUserById(userId) {
        return this.http.get(`/shared/idm-app/user/${userId}`);
    }

    getUserDetailById(userId) {
        var myObservable = new Rx.Subject();
        this.getUserById(userId).subscribe(
            user => {
                this.setUserCompanyFacilityObjsInfoById(user["defaultCompanyFacility"], user["assignedCompanyFacilities"], function(){
                    myObservable.next(user);
                });
            },
            (err: HttpErrorResponse) => {
                myObservable.error(err);
            }
        );
        return myObservable;
    }

    setUserCompanyFacilityObjsInfoById(defaultCf, assignedCfs, cbFun) {
        var companyIds = _.uniq(_.map(assignedCfs, "companyId"));
        var facililtyIds = _.uniq(_.map(assignedCfs, "facilityId"));
        var observable = forkJoin(
            this.facilityService.searchFacility({ ids: facililtyIds }),
            this.companyService.searchCompany({ ids: companyIds })
        );
        observable.subscribe(([res1, res2]) => {
            var orgs = _.concat(res1,res2);
            this.utilService.extractOrganaizationName(orgs);
            var orgsMap = _.keyBy(orgs, function (o) { return o["id"]; });
            _.forEach(assignedCfs, cf => {
                this.setCompanyFacilityObjInfoById(cf, orgsMap);
            });
            this.setCompanyFacilityObjInfoById(defaultCf, orgsMap);
            cbFun();
        });
       
    };

  
    private setCompanyFacilityObjInfoById(cf, orgsMap) {
        if (!cf) return;
        if (cf.companyId) {
            cf.company = orgsMap[cf.companyId];
        }
        if (cf.facilityId) {
            cf.facility = orgsMap[cf.facilityId];
        }
    }
      
}