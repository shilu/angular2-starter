export const MODULE_CONFIG: Object[] = [
    {
		name: 'dashboard',
		iconClass: 'fa fa-tachometer',
		label: 'Dashboard',
		moduleName: 'dashboardModule',
		path:'dashboard',
	},{
		name: 'wms',
		iconClass: 'material-icons',
		iconText: 'local_shipping',
		label: 'Warehouse (WMS)',
		moduleName: 'wmsModule',
		path:'wms',
		// roles: ['WAREHOUSE_LABOR']
		// permissions: 'wms'
	}, {
		name: 'inventory',
		iconClass: 'fa fa-cubes',
		label: 'Inventory',
		moduleName: 'inventoryModule',
		path:'inventory',
		// roles: ['WAREHOUSE_LABOR']
	}, {
		name: 'fd',
		iconClass: 'fa fa-database',
		label: 'Foundation Data',
		moduleName: 'fdModule',
		path:'fd',
		// roles: ['WAREHOUSE_LABOR']
	}, {
		name: 'cf',
		iconClass: 'fa fa-database',
		label: 'Company & Facility',
		moduleName: 'cfModule',
		path:'cf',
		// roles: ['WAREHOUSE_LABOR']
	}, {
		name: 'gis',
		//templateUrl : 'gis/resources/template/resources.html',
		controller: 'gis.MainPageController',
		iconClass: 'material-icons',
		iconText: 'language',
		label: 'GIS',
		moduleName: 'gisModule',
		path:'gis',
		// roles: ['WAREHOUSE_LABOR']
	}, {
		name: 'admin',
		iconClass: 'fa fa-cog',
		label: 'Admin',
		moduleName: 'adminModule',
		path:'admin',
		// roles: ['WAREHOUSE_SUPERVISOR']
	}, {
		name: 'user',
		iconClass: 'fa fa-user',
		label: 'User',
		moduleName: 'userModule',
		path:'user',
		// roles: ['WAREHOUSE_SUPERVISOR']
	},{
		name: 'rc',
		iconClass: 'fa fa-align-justify',
		label: 'Report Center',
		moduleName: 'rcModule',
		path:'rc',
		// roles: ['WAREHOUSE_SUPERVISOR']
	}];
