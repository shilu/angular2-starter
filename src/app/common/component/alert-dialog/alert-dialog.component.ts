import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'alert-dialog',
    templateUrl: 'alert-dialog.component.html'
  })
  export class AlertDialogComponent {
    dialogMessage:string;
    dialogTitle:string;
    isConfirm:boolean = false;
    constructor(public dialogRef: MatDialogRef<AlertDialogComponent> ) { }

  
  }